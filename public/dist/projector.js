/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./public/js/projector/maps.js":
/*!*************************************!*\
  !*** ./public/js/projector/maps.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   faceEnter: () => (/* binding */ faceEnter),\n/* harmony export */   faceLeave: () => (/* binding */ faceLeave),\n/* harmony export */   fromProject: () => (/* binding */ fromProject),\n/* harmony export */   toProject: () => (/* binding */ toProject)\n/* harmony export */ });\n/* harmony import */ var _projector_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./projector.js */ \"./public/js/projector/projector.js\");\n\r\n\r\nlet satelliteWindow = document.querySelector('#satelliteWindow');\r\nlet satellite = document.querySelector('#satellite');\r\n\r\n// const mapCoordinates = {\r\n//   tl: {y: 48.272330, x: 16.282738},\r\n//   bl: {y: 48.141900, x: 16.282738},\r\n//   br: {y: 48.141900, x: 16.478468},\r\n//   tr: {y: 48.272330, x: 16.478468}\r\n// }\r\n\r\nfunction faceEnter(id) {\r\n\r\n}\r\n\r\nfunction faceLeave(id) {\r\n  satellite.style.transform = `scale(1) translate3D(0%, 0%, 0)`;\r\n}\r\n\r\nfunction toProject(id) {\r\n  // let x = (Math.random() * 20) - 10;\r\n  // let y = (Math.random() * 20) - 10;\r\n\r\n  let x = _projector_js__WEBPACK_IMPORTED_MODULE_0__.projects[id].translate.x;\r\n  let y = _projector_js__WEBPACK_IMPORTED_MODULE_0__.projects[id].translate.y;\r\n\r\n  satellite.style.transform = `scale(10) translate3D(${x}%, ${y}%, 0)`;\r\n}\r\n\r\nfunction fromProject(id) {\r\n  \r\n}\n\n//# sourceURL=webpack://websocket/./public/js/projector/maps.js?");

/***/ }),

/***/ "./public/js/projector/overlay.js":
/*!****************************************!*\
  !*** ./public/js/projector/overlay.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   faceEnter: () => (/* binding */ faceEnter),\n/* harmony export */   faceLeave: () => (/* binding */ faceLeave)\n/* harmony export */ });\nconst overlayIn = document.querySelector('#overlayIn');\r\nconst overlayOut = document.querySelector('#overlayOut');\r\n\r\nfunction faceEnter() {\r\n  overlayOut.pause();\r\n  overlayOut.currentTime = 0;\r\n  overlayOut.classList.remove('isVisible');\r\n  overlayIn.classList.add('isVisible');\r\n  overlayIn.play();\r\n}\r\n\r\nfunction faceLeave() {\r\n  overlayIn.pause();\r\n  overlayIn.currentTime = 0;\r\n  overlayIn.classList.remove('isVisible');\r\n  overlayOut.classList.add('isVisible');\r\n  overlayOut.play();\r\n}\n\n//# sourceURL=webpack://websocket/./public/js/projector/overlay.js?");

/***/ }),

/***/ "./public/js/projector/project.js":
/*!****************************************!*\
  !*** ./public/js/projector/project.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Project)\n/* harmony export */ });\nconst projection = document.querySelector('#projection');\r\n\r\nlet zIndex = 1;\r\n\r\nconst mapCoordinates = {\r\n  longMin: 16.282738, \r\n  longMax: 16.478468,\r\n  latMin: 48.141900,\r\n  latMax: 48.272330\r\n};\r\n\r\nfunction rangeValue(val, inMin, inMax, outMin, outMax) {\r\n  return (val - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;\r\n}\r\n\r\nclass Project {\r\n  constructor(properties, index) {\r\n    Object.assign(this, properties);\r\n    this.index = index;\r\n    this.translate = {};\r\n    \r\n    this.format();\r\n    this.setupDrone();\r\n  }\r\n\r\n  init() {\r\n    this.generateHtml();\r\n    this.appendHtml();\r\n  }\r\n\r\n  format() {\r\n    this.abbr = `SP-${this.placement}-${this.index.toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping: false})}`;\r\n\r\n    this.position = this.position.split(\",\").map(item => item.trim());\r\n\r\n    this.translate.y = rangeValue(this.position[0], mapCoordinates.latMin, mapCoordinates.latMax, -50, 50);\r\n    this.translate.x = rangeValue(this.position[1], mapCoordinates.longMin, mapCoordinates.longMax, 50, -50);\r\n  };\r\n\r\n  setupDrone() {\r\n    if (this.video == 'X') {\r\n      console.log(this.title);\r\n      this.drone = this.title + '.mp4';\r\n    } else if (this.video) {\r\n      this.drone = this.video + '.mp4';\r\n    } else {\r\n      this.drone = 'a-1.mp4';\r\n    }\r\n  }\r\n\r\n  generateHtml() {\r\n\r\n    this.html = {};\r\n\r\n    this.html.title = document.createElement('h3');\r\n    this.html.title.classList = 'project__title';\r\n    this.html.title.innerHTML = this.title;\r\n\r\n    this.html.placement = document.createElement('span');\r\n    this.html.placement.classList = 'project__placement';\r\n    this.html.placement.innerHTML = this.placement;\r\n\r\n    this.html.videoWindow = document.createElement('div');\r\n    this.html.videoWindow.classList = 'project__window';\r\n\r\n    this.html.video = document.createElement('video');\r\n    this.html.video.classList = 'project__video';\r\n    this.html.video.src = `./assets/drone/${this.drone}`;\r\n    this.html.video.muted = true;\r\n    this.html.video.loop = true;\r\n    this.html.video.preload = 'metadata';\r\n    this.html.videoWindow.appendChild(this.html.video);\r\n\r\n    this.html.wrapper = document.createElement('div');\r\n    this.html.wrapper.classList = 'project';\r\n\r\n    this.html.wrapper.appendChild(this.html.title);\r\n    this.html.wrapper.appendChild(this.html.placement);\r\n    this.html.wrapper.appendChild(this.html.videoWindow);\r\n  }\r\n\r\n  appendHtml() {\r\n    let fragment = document.createDocumentFragment();\r\n    fragment.appendChild(this.html.wrapper)\r\n    projection.appendChild(fragment);\r\n  }\r\n\r\n  openProject() {\r\n    this.html.wrapper.style.zIndex = zIndex;\r\n    zIndex++;\r\n    let previousProject = document.querySelector('.project.isOpen');\r\n    if (previousProject) {\r\n      setTimeout(function() {\r\n        previousProject.classList.remove('isOpen')\r\n      }, 1000);\r\n    }\r\n    this.html.wrapper.classList.add('isOpen');\r\n  }\r\n  \r\n  closeProject() {\r\n    this.html.wrapper.classList.remove('isOpen');\r\n  }\r\n}\n\n//# sourceURL=webpack://websocket/./public/js/projector/project.js?");

/***/ }),

/***/ "./public/js/projector/projector.js":
/*!******************************************!*\
  !*** ./public/js/projector/projector.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   projects: () => (/* binding */ projects)\n/* harmony export */ });\n/* harmony import */ var _project_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./project.js */ \"./public/js/projector/project.js\");\n/* harmony import */ var _socketio_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./socketio.js */ \"./public/js/projector/socketio.js\");\n\r\n\r\n\r\nlet projects = [];\r\n\r\nfetch('../json/projects.json').then(response => {\r\n  \r\n  return response.json();\r\n\r\n}).then(items => {  \r\n  \r\n  for (let index = 0; index < items.length; index++) {\r\n    let project = new _project_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"](items[index], index);\r\n    projects.push(project);\r\n  }\r\n\r\n  for (const project of projects) {\r\n    project.init();\r\n  }\r\n\r\n}).catch(error => {\r\n  console.error(error);\r\n})\r\n\r\n\r\nlet h1 = document.querySelector('h1');\r\nlet audio = document.querySelector('audio');\r\n\r\nh1.addEventListener('click', function() {\r\n  audio.play();\r\n})\n\n//# sourceURL=webpack://websocket/./public/js/projector/projector.js?");

/***/ }),

/***/ "./public/js/projector/socketio.js":
/*!*****************************************!*\
  !*** ./public/js/projector/socketio.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   socket: () => (/* binding */ socket)\n/* harmony export */ });\n/* harmony import */ var _project_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./project.js */ \"./public/js/projector/project.js\");\n/* harmony import */ var _projector_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./projector.js */ \"./public/js/projector/projector.js\");\n/* harmony import */ var _style_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.js */ \"./public/js/projector/style.js\");\n/* harmony import */ var _sound_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sound.js */ \"./public/js/projector/sound.js\");\n/* harmony import */ var _video_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./video.js */ \"./public/js/projector/video.js\");\n/* harmony import */ var _overlay_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./overlay.js */ \"./public/js/projector/overlay.js\");\n/* harmony import */ var _maps_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./maps.js */ \"./public/js/projector/maps.js\");\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nconst socket = io();\r\n\r\nsetTimeout(function() {\r\n  (0,_sound_js__WEBPACK_IMPORTED_MODULE_3__.faceEnter)();\r\n  (0,_video_js__WEBPACK_IMPORTED_MODULE_4__.faceEnter)();\r\n  (0,_overlay_js__WEBPACK_IMPORTED_MODULE_5__.faceEnter)();\r\n  (0,_style_js__WEBPACK_IMPORTED_MODULE_2__.faceEnter)();\r\n}, 2000);\r\n\r\nsocket.on('faceEnterProjectorIn', (data) => {\r\n  console.log('enter');\r\n  (0,_sound_js__WEBPACK_IMPORTED_MODULE_3__.faceEnter)();\r\n  (0,_video_js__WEBPACK_IMPORTED_MODULE_4__.faceEnter)();\r\n  (0,_overlay_js__WEBPACK_IMPORTED_MODULE_5__.faceEnter)();\r\n  (0,_style_js__WEBPACK_IMPORTED_MODULE_2__.faceEnter)();\r\n})\r\n\r\nsocket.on('faceLeaveProjectorIn', (data) => {\r\n  ;(0,_sound_js__WEBPACK_IMPORTED_MODULE_3__.faceLeave)();\r\n  (0,_video_js__WEBPACK_IMPORTED_MODULE_4__.faceLeave)();\r\n  (0,_overlay_js__WEBPACK_IMPORTED_MODULE_5__.faceLeave)();\r\n  (0,_style_js__WEBPACK_IMPORTED_MODULE_2__.faceLeave)();\r\n})\r\n\r\nsocket.on('openProjectIn', (data) => {\r\n  _projector_js__WEBPACK_IMPORTED_MODULE_1__.projects[data.project].openProject();\r\n  (0,_sound_js__WEBPACK_IMPORTED_MODULE_3__.playProject)(data.project);\r\n  (0,_video_js__WEBPACK_IMPORTED_MODULE_4__.playProject)(data.project);\r\n  (0,_maps_js__WEBPACK_IMPORTED_MODULE_6__.toProject)(data.project);\r\n})\r\n\r\nsocket.on('closeProjectIn', (data) => {\r\n  _projector_js__WEBPACK_IMPORTED_MODULE_1__.projects[data.project].closeProject();\r\n  (0,_sound_js__WEBPACK_IMPORTED_MODULE_3__.stopProject)(data.project);\r\n  (0,_video_js__WEBPACK_IMPORTED_MODULE_4__.stopProject)(data.project);\r\n  (0,_maps_js__WEBPACK_IMPORTED_MODULE_6__.fromProject)(data.project);\r\n})\r\n\r\nsocket.on('changeProjectIn', (data) => {\r\n  setTimeout(function() {\r\n    _projector_js__WEBPACK_IMPORTED_MODULE_1__.projects[data.project].closeProject();\r\n  }, 1000);\r\n  (0,_sound_js__WEBPACK_IMPORTED_MODULE_3__.stopProject)(data.project);\r\n  (0,_video_js__WEBPACK_IMPORTED_MODULE_4__.stopProject)(data.project);\r\n})\r\n\r\n\n\n//# sourceURL=webpack://websocket/./public/js/projector/socketio.js?");

/***/ }),

/***/ "./public/js/projector/sound.js":
/*!**************************************!*\
  !*** ./public/js/projector/sound.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   faceEnter: () => (/* binding */ faceEnter),\n/* harmony export */   faceLeave: () => (/* binding */ faceLeave),\n/* harmony export */   playProject: () => (/* binding */ playProject),\n/* harmony export */   stopProject: () => (/* binding */ stopProject)\n/* harmony export */ });\n/* harmony import */ var _projector_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./projector.js */ \"./public/js/projector/projector.js\");\n\r\n\r\nconst activateMedia = document.querySelector('#activateMedia');\r\nconst toOne = document.querySelector('#toOne');\r\nconst toZero = document.querySelector('#toZero');\r\n\r\n\r\nlet isMediaActive = false;\r\n\r\nlet samplePaths = [\r\n  {\r\n    key: 'a-1',\r\n    path: './assets/sounds/a-1.mp3'\r\n  },\r\n  {\r\n    key: 'a-2',\r\n    path: './assets/sounds/a-2.mp3'\r\n  },\r\n  {\r\n    key: 'a-5',\r\n    path: './assets/sounds/a-5.mp3'\r\n  },\r\n  {\r\n    key: 'a-8',\r\n    path: './assets/sounds/a-8.mp3'\r\n  },\r\n  {\r\n    key: 'a-9',\r\n    path: './assets/sounds/a-9.mp3'\r\n  },\r\n  {\r\n    key: 'a-10',\r\n    path: './assets/sounds/a-10.mp3'\r\n  },\r\n  {\r\n    key: 'a-11',\r\n    path: './assets/sounds/a-11.mp3'\r\n  },\r\n  {\r\n    key: 'a-12',\r\n    path: './assets/sounds/a-12.mp3'\r\n  },\r\n  {\r\n    key: 'a-13',\r\n    path: './assets/sounds/a-13.mp3'\r\n  },\r\n  {\r\n    key: 'a-15',\r\n    path: './assets/sounds/a-15.mp3'\r\n  },\r\n  {\r\n    key: 'a-16',\r\n    path: './assets/sounds/a-16.mp3'\r\n  },\r\n  {\r\n    key: 'a-17',\r\n    path: './assets/sounds/a-17.mp3'\r\n  },\r\n  {\r\n    key: 'a-18',\r\n    path: './assets/sounds/a-18.mp3'\r\n  },\r\n  {\r\n    key: 'a-19',\r\n    path: './assets/sounds/a-19.mp3'\r\n  },\r\n  {\r\n    key: 'a-20',\r\n    path: './assets/sounds/a-20.mp3'\r\n  },\r\n  {\r\n    key: 'a-22',\r\n    path: './assets/sounds/a-22.mp3'\r\n  },\r\n  {\r\n    key: 'a-23',\r\n    path: './assets/sounds/a-23.mp3'\r\n  },\r\n  {\r\n    key: 'a-24',\r\n    path: './assets/sounds/a-24.mp3'\r\n  },\r\n  {\r\n    key: 'a-25',\r\n    path: './assets/sounds/a-25.mp3'\r\n  },\r\n  {\r\n    key: 'a-28',\r\n    path: './assets/sounds/a-28.mp3'\r\n  },\r\n  {\r\n    key: 'a-29',\r\n    path: './assets/sounds/a-29.mp3'\r\n  },\r\n  {\r\n    key: 'a-30',\r\n    path: './assets/sounds/a-30.mp3'\r\n  },\r\n  {\r\n    key: 'a-31',\r\n    path: './assets/sounds/a-31.mp3'\r\n  },\r\n  {\r\n    key: 'a-32',\r\n    path: './assets/sounds/a-32.mp3'\r\n  },\r\n  {\r\n    key: 'a-33',\r\n    path: './assets/sounds/a-33.mp3'\r\n  },\r\n  {\r\n    key: 'a-34',\r\n    path: './assets/sounds/a-34.mp3'\r\n  },\r\n  {\r\n    key: 'a-35',\r\n    path: './assets/sounds/a-35.mp3'\r\n  },\r\n  {\r\n    key: 'b-1',\r\n    path: './assets/sounds/b-1.mp3'\r\n  },\r\n  {\r\n    key: 'b-2',\r\n    path: './assets/sounds/b-2.mp3'\r\n  },\r\n  {\r\n    key: 'b-5',\r\n    path: './assets/sounds/b-5.mp3'\r\n  },\r\n  {\r\n    key: 'b-6',\r\n    path: './assets/sounds/b-6.mp3'\r\n  },\r\n  {\r\n    key: 'b-10',\r\n    path: './assets/sounds/b-10.mp3'\r\n  },\r\n  {\r\n    key: 'b-11',\r\n    path: './assets/sounds/b-11.mp3'\r\n  },\r\n  {\r\n    key: 'b-14',\r\n    path: './assets/sounds/b-14.mp3'\r\n  },\r\n  {\r\n    key: 'b-17',\r\n    path: './assets/sounds/b-17.mp3'\r\n  },\r\n  {\r\n    key: 'b-18',\r\n    path: './assets/sounds/b-18.mp3'\r\n  },\r\n  {\r\n    key: 'b-21',\r\n    path: './assets/sounds/b-21.mp3'\r\n  },\r\n  {\r\n    key: 'b-22',\r\n    path: './assets/sounds/b-22.mp3'\r\n  },\r\n  {\r\n    key: 'b-25',\r\n    path: './assets/sounds/b-25.mp3'\r\n  },\r\n  {\r\n    key: 'b-27',\r\n    path: './assets/sounds/b-27.mp3'\r\n  },\r\n  {\r\n    key: 'b-29',\r\n    path: './assets/sounds/b-29.mp3'\r\n  },\r\n  {\r\n    key: 'c-1',\r\n    path: './assets/sounds/c-1.mp3'\r\n  },\r\n  {\r\n    key: 'c-2',\r\n    path: './assets/sounds/c-2.mp3'\r\n  },\r\n  {\r\n    key: 'c-3',\r\n    path: './assets/sounds/c-3.mp3'\r\n  },\r\n  {\r\n    key: 'c-5',\r\n    path: './assets/sounds/c-5.mp3'\r\n  },\r\n  {\r\n    key: 'c-9',\r\n    path: './assets/sounds/c-9.mp3'\r\n  },\r\n  {\r\n    key: 'c-10',\r\n    path: './assets/sounds/c-10.mp3'\r\n  },\r\n  {\r\n    key: 'c-11',\r\n    path: './assets/sounds/c-11.mp3'\r\n  },\r\n  {\r\n    key: 'c-12',\r\n    path: './assets/sounds/c-12.mp3'\r\n  },\r\n  {\r\n    key: 'c-14',\r\n    path: './assets/sounds/c-14.mp3'\r\n  },\r\n  {\r\n    key: 'c-18',\r\n    path: './assets/sounds/c-18.mp3'\r\n  },\r\n  {\r\n    key: 'c-21',\r\n    path: './assets/sounds/c-21.mp3'\r\n  },\r\n];\r\n\r\nconst audioCtx = new AudioContext();\r\nlet samples;\r\nlet playing = [];\r\nlet playingPrevious;\r\n\r\nactivateMedia.addEventListener('click', function() {\r\n  \r\n  if (audioCtx.state == \"suspended\") {\r\n    audioCtx.resume();\r\n  }\r\n\r\n  isMediaActive = true;\r\n  activateMedia.remove();\r\n})\r\n\r\ntoOne.addEventListener('click', function() {\r\n  // gainNode.gain.linearRampToValueAtTime(1.0, audioCtx.currentTime + 2);\r\n})\r\n\r\ntoZero.addEventListener('click', function() {\r\n  // gainNode.gain.linearRampToValueAtTime(0, audioCtx.currentTime + 2);\r\n  stopSample(playing);\r\n})\r\n\r\nasync function getFile(filePath) {\r\n  const response = await fetch(filePath);\r\n  const arrayBuffer = await response.arrayBuffer();\r\n  const audioBuffer = await audioCtx.decodeAudioData(arrayBuffer);\r\n\r\n  return audioBuffer;\r\n}\r\n\r\nasync function setupSamples(paths) {\r\n  const audioBuffers = [];\r\n\r\n  for (let path of paths) {\r\n    let sample = await getFile(path.path);\r\n\r\n    let object = {\r\n      key: path.key,\r\n      sample: sample\r\n    }\r\n    audioBuffers.push(object);\r\n  }\r\n\r\n  return audioBuffers;\r\n}\r\n\r\nfunction playSample(audioBuffer) {\r\n  const sampleSource = audioCtx.createBufferSource();\r\n  sampleSource.buffer = audioBuffer.sample;\r\n\r\n  let gainNode = audioCtx.createGain();\r\n  \r\n  gainNode.gain.setValueAtTime(0, audioCtx.currentTime);\r\n  gainNode.gain.linearRampToValueAtTime(1, audioCtx.currentTime + 2);\r\n\r\n  gainNode.connect(audioCtx.destination);\r\n  sampleSource.connect(gainNode);\r\n  \r\n  sampleSource.start(0);\r\n\r\n  return [sampleSource, gainNode];\r\n}\r\n\r\nfunction stopSample(playing) {\r\n  let sampleSource = playing[0];\r\n  let gainNode = playing[1];\r\n  gainNode.gain.setValueAtTime(1, audioCtx.currentTime);\r\n  gainNode.gain.linearRampToValueAtTime(0, audioCtx.currentTime + 2);\r\n\r\n  setTimeout(function() {\r\n    sampleSource.stop();\r\n    gainNode.disconnect();\r\n    sampleSource.disconnect();\r\n  }, 2000);\r\n}\r\n\r\nsetupSamples(samplePaths).then(function(response) {\r\n  samples = response;\r\n  console.log('sound-ready');\r\n  console.log(samples);\r\n})\r\n\r\n\r\n\r\n\r\n\r\n\r\nfunction faceEnter() {\r\n  if (isMediaActive) {\r\n  }\r\n}\r\n\r\nfunction faceLeave() {\r\n  if (isMediaActive) {\r\n  }\r\n}\r\n\r\nlet sampleCounter = 0;\r\n\r\nfunction playProject(id) {\r\n  if (isMediaActive) {\r\n    let key;\r\n\r\n    console.log(_projector_js__WEBPACK_IMPORTED_MODULE_0__.projects[id].sound);\r\n\r\n    if (_projector_js__WEBPACK_IMPORTED_MODULE_0__.projects[id].sound == 'X') {\r\n      key = _projector_js__WEBPACK_IMPORTED_MODULE_0__.projects[id].title;\r\n    } else if (_projector_js__WEBPACK_IMPORTED_MODULE_0__.projects[id].sound) {\r\n      key = _projector_js__WEBPACK_IMPORTED_MODULE_0__.projects[id].sound;\r\n    } else {\r\n      key = 'a-39';39    }\r\n\r\n    // console.log(key);\r\n\r\n    let wrapper = samples.find(obj => {\r\n      return obj.key === key\r\n    })\r\n\r\n    console.log(wrapper);\r\n\r\n    playing = playSample(wrapper);\r\n    sampleCounter++;\r\n\r\n    // setTimeout(function() {\r\n    //   playingPrevious = playing;\r\n    // }, 100);\r\n  }\r\n}\r\n\r\nfunction stopProject(id) {\r\n  if (isMediaActive) {\r\n    stopSample(playing);\r\n  }\r\n}\n\n//# sourceURL=webpack://websocket/./public/js/projector/sound.js?");

/***/ }),

/***/ "./public/js/projector/style.js":
/*!**************************************!*\
  !*** ./public/js/projector/style.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   faceEnter: () => (/* binding */ faceEnter),\n/* harmony export */   faceLeave: () => (/* binding */ faceLeave)\n/* harmony export */ });\nconst body = document.querySelector('body');\r\nconst h1 = document.querySelector('h1');\r\n\r\nfunction faceEnter() {\r\n  body.classList.add('isEntered');\r\n}\r\n\r\nfunction faceLeave() {\r\n  body.classList.remove('isEntered');\r\n}\n\n//# sourceURL=webpack://websocket/./public/js/projector/style.js?");

/***/ }),

/***/ "./public/js/projector/video.js":
/*!**************************************!*\
  !*** ./public/js/projector/video.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   faceEnter: () => (/* binding */ faceEnter),\n/* harmony export */   faceLeave: () => (/* binding */ faceLeave),\n/* harmony export */   playProject: () => (/* binding */ playProject),\n/* harmony export */   stopProject: () => (/* binding */ stopProject)\n/* harmony export */ });\n/* harmony import */ var _projector_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./projector.js */ \"./public/js/projector/projector.js\");\n\r\n\r\nconst activateMedia = document.querySelector('#activateMedia');\r\n\r\nlet isMediaActive = false;\r\n\r\nlet playing;\r\nlet playingPrevious;\r\n\r\nactivateMedia.addEventListener('click', function() {\r\n  isMediaActive = true;\r\n})\r\n\r\nfunction playVideo(id) {\r\n  let video = _projector_js__WEBPACK_IMPORTED_MODULE_0__.projects[id].html.video;\r\n  video.play();\r\n\r\n  return video;\r\n}\r\n\r\nfunction stopVideo(playing) {\r\n  let video = playing;\r\n  \r\n  setTimeout(function() {\r\n    video.pause();\r\n    video.currentTime = 0;\r\n  }, 2000);\r\n}\r\n\r\n\r\n\r\nfunction faceEnter() {\r\n  if (isMediaActive) {\r\n  }\r\n}\r\n\r\nfunction faceLeave() {\r\n  if (isMediaActive) {\r\n  }\r\n}\r\n\r\nfunction playProject(id) {\r\n  if (isMediaActive) {\r\n    playing = playVideo(id);\r\n  }\r\n}\r\n\r\nfunction stopProject(id) {\r\n  if (isMediaActive) {\r\n    stopVideo(playing);\r\n  }\r\n}\n\n//# sourceURL=webpack://websocket/./public/js/projector/video.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./public/js/projector/projector.js");
/******/ 	
/******/ })()
;
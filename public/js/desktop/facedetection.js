import {
  FaceDetector,
  FilesetResolver
} from "@mediapipe/tasks-vision"
import { socket } from "./socketio.js";
import { faceEnter as faceEnterThree, faceLeave as faceLeaveThree } from "./scene.js";
import { faceEnter as faceEnterCSS, faceLeave as faceLeaveCSS } from "./style.js";

let faceDetector;
let runningMode = "VIDEO";
export let faceDetected = false;
let faceDetectionTimeout = false;
let faceDetectionLeaveTimeout = false;

let leaveTimeout;

// Initialize the object detector
export const initializefaceDetector = async () => {
  const vision = await FilesetResolver.forVisionTasks(
    "https://cdn.jsdelivr.net/npm/@mediapipe/tasks-vision@0.10.0/wasm"
  )
  faceDetector = await FaceDetector.createFromOptions(vision, {
    baseOptions: {
      modelAssetPath: `https://storage.googleapis.com/mediapipe-models/face_detector/blaze_face_short_range/float16/1/blaze_face_short_range.tflite`,
      delegate: "GPU"
    },
    runningMode: runningMode
  })
}
initializefaceDetector()

let video = document.getElementById("webcam")

// Keep a reference of all the child elements we create
// so we can remove them easily on each render.
var children = []


// Enable the live webcam view and start detection.
export async function enableCam(event) {
  console.log('webcam enabled');
  if (!faceDetector) {
    alert("Face Detector is still loading. Please try again..")
    return
  }

  // getUsermedia parameters
  const constraints = {
    video: true
  }

  // Activate the webcam stream.
  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(function(stream) {
      video.srcObject = stream
      video.addEventListener("loadeddata", predictWebcam)
    })
    .catch(err => {
      console.error(err)
    })
}

let lastVideoTime = -1
async function predictWebcam() {
  let startTimeMs = performance.now()

  // Detect faces using detectForVideo
  if (video.currentTime !== lastVideoTime) {
    lastVideoTime = video.currentTime
    const detections = faceDetector.detectForVideo(video, startTimeMs)
      .detections
    displayVideoDetections(detections)
  }

  // Call this function again to keep predicting when the browser is ready
  window.requestAnimationFrame(predictWebcam)
}

function displayVideoDetections(detections) {
  // Remove any highlighting from previous frame.

  // for (let child of children) {
  //   liveView.removeChild(child)
  // }
  // children.splice(0)

  // Iterate through predictions and draw them to the live view

  function handleLeave() {
    faceLeaveThree();
    faceLeaveCSS();
    faceDetectionTimeout = true;

    socket.emit('faceLeaveProjectorOut', {isEntered: false});

    setTimeout(function() {
      faceDetectionTimeout = false;
    }, 3000);
  }

  if (detections.length == 0 && faceDetected == true) {
    if (!faceDetectionTimeout) {
      faceDetected = false;
      faceDetectionLeaveTimeout = true;
      
      leaveTimeout = setTimeout(function() {
        if (detections.length == 0 && !faceDetected) {
          faceDetectionLeaveTimeout = false;
          handleLeave();
        }
      }, 3000);
    } 
  } else if (detections.length != 0 && faceDetected == false) {
    if (!faceDetectionTimeout) {
      faceDetected = true;
      if (!faceDetectionLeaveTimeout) {
        faceEnterThree();
        faceEnterCSS();
        faceDetectionTimeout = true;
  
        socket.emit('faceEnterProjectorOut', {isEntered: true});
        
        setTimeout(function() {
          faceDetectionTimeout = false;
        }, 3000);
      }
    } 
  }

  if (faceDetectionLeaveTimeout && detections.length != 0) {
    clearTimeout(leaveTimeout);
    faceDetectionLeaveTimeout = false;
  }

  // if (detections.length == 0 && faceDetected == false) {
  //   faceEnterThree();
  //   faceEnterCSS();
  //   faceDetected = true;
  //   socket.emit('faceEnterProjectorOut', {isEntered: true});
  // }

  // for (let detection of detections) {
  //   const highlighter = document.createElement("div")
  //   highlighter.setAttribute("class", "highlighter")
  //   highlighter.style =
  //     "left: " +
  //     (video.offsetWidth -
  //       detection.boundingBox.width -
  //       detection.boundingBox.originX) +
  //     "px;" +
  //     "top: " +
  //     detection.boundingBox.originY +
  //     "px;" +
  //     "width: " +
  //     (detection.boundingBox.width - 10) +
  //     "px;" +
  //     "height: " +
  //     detection.boundingBox.height +
  //     "px;"

  //   liveView.appendChild(highlighter)

  //   children.push(highlighter)
  //   for (let keypoint of detection.keypoints) {
  //     const keypointEl = document.createElement("spam")
  //     keypointEl.className = "key-point"
  //     keypointEl.style.top = `${keypoint.y * video.offsetHeight - 3}px`
  //     keypointEl.style.left = `${video.offsetWidth -
  //       keypoint.x * video.offsetWidth -
  //       3}px`
  //     liveView.appendChild(keypointEl)
  //     children.push(keypointEl)
  //   }
  // }
}

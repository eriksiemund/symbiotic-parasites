import Project from './project.js';
import { socket } from './socketio.js'
import {init as initThreeScene, animate as animateThreeScene, raycasting, bringCameraIntoPosition} from './scene.js';
import { initializefaceDetector, enableCam, faceDetected } from './facedetection.js';
import { mouseenterProject as mouseenterProjectCSS, mouseleaveProject as mouseleaveProjectCSS } from './style.js';

let projects = [];
let previousProject;
let previousHoverProject;
let canvas;

let isMouseDown = false;
let isMouseDrag = false;


initThreeScene();
initializefaceDetector()

fetch('../json/projects.json').then(response => {
  
  return response.json();

}).then(items => {  
  
  for (let index = 0; index < items.length; index++) {
    let project = new Project(items[index], index);
    projects.push(project);
  }

  for (const project of projects) {
    project.init();
  }

  setTimeout(function() {
    animateThreeScene();
    bringCameraIntoPosition();
  }, 500);


}).catch(error => {
  console.error(error);
})

let pointerPosition = {};

window.addEventListener('mousedown', function(event) {
  pointerPosition.x = event.clientX;
  pointerPosition.y = event.clientY;

  isMouseDown = true;
});

window.addEventListener('mouseup', function(event) {
  if (pointerPosition.x == event.clientX && pointerPosition.y == event.clientY) {
    if (previousProject) {
      previousProject.closeProject();
    }
    let id = raycasting(event, 'click');
    if (id) {
      projects[id].openProject();
      previousProject = projects[id];
    }
  }

  isMouseDown = false;
  isMouseDrag = false;
});

window.addEventListener('mousemove', function(event) {
  let id;

  if (isMouseDown) {
    if (isMouseDrag) {
      id = raycasting(event, 'drag');
    } else {
      if (previousProject) {
        previousProject.closeProject();
        previousProject = null;
      }

      id = raycasting(event, 'dragStart');
    }

    isMouseDrag = true;
  } else {
    id = raycasting(event, 'hover');
    
    if (id && previousHoverProject != projects[id]) {
      projects[id].mouseenterProject();
      previousHoverProject = projects[id];
      mouseenterProjectCSS();
    } else if (!id && previousHoverProject) {
      previousHoverProject.mouseleaveProject();
      previousHoverProject = null;
      mouseleaveProjectCSS();
    }

    isMouseDrag = false;
  }


})

window.addEventListener('wheel', function(event) {
  if (previousProject) {
    previousProject.closeProject();
    previousProject = null;
  }
  
  let id;
  id = raycasting(event, 'wheel');
});

setTimeout(function() {
  enableCam();
}, 5000);



import {setupProject as setupThreeProject} from './scene.js';

const body = document.querySelector('body');

export default class Project {
  constructor(properties, index) {
    Object.assign(this, properties);
    this.index = index;
    
    this.format();
  }

  
  init() {
    this.generateHtml();
    this.appendHtml();
    this.generateThree();
  }

  format() {
    let number = Number.parseInt(this.title.split('-')[1]);
    this.abbr = `SP-${this.placement}-${number.toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping: false})}`;
  }

  generateHtml() {

    this.html = {};

    this.html.title = document.createElement('h3');
    this.html.title.classList = 'project__title';
    this.html.title.innerHTML = this.abbr;

    this.html.placement = document.createElement('span');
    this.html.placement.classList = 'project__placement';
    if (this.placement == 'A') {
      this.html.placement.innerHTML = 'Above';
    } else if (this.placement == 'B') {
      this.html.placement.innerHTML = 'Between';
    } else if (this.placement == 'C') {
      this.html.placement.innerHTML = 'Connected';
    }
    
    this.html.difficulty = document.createElement('span');
    this.html.difficulty.classList = 'project__difficulty';
    this.html.difficulty.innerHTML = this.complexity;

    this.html.materials = document.createElement('span');
    this.html.materials.classList = 'project__materials';
    this.html.materials.innerHTML = this.materials;
    this.html.wrapper = document.createElement('div');
    this.html.wrapper.classList = 'project';

    this.html.wrapper.appendChild(this.html.title);
    this.html.wrapper.appendChild(this.html.placement);
    this.html.wrapper.appendChild(this.html.difficulty);
    this.html.wrapper.appendChild(this.html.materials);
  }

  appendHtml() {
    let fragment = document.createDocumentFragment();
    fragment.appendChild(this.html.wrapper)
    body.appendChild(fragment);
  }

  generateThree() {
    setupThreeProject(this);
  }

  openProject() {
    // let previousProject = document.querySelector('.project.isOpen');
    // if (previousProject) {
    //   previousProject.classList.remove('isOpen')
    // }
    this.html.wrapper.classList.add('isOpen');
  }
  
  closeProject() {
    this.html.wrapper.classList.remove('isOpen');
  }

  mouseenterProject() {
    this.html.wrapper.classList.add('isHover');
  }
  
  mouseleaveProject() {
    this.html.wrapper.classList.remove('isHover');
  }
}
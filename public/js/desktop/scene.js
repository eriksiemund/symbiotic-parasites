import * as THREE from 'three';
import { OrbitControls } from './OrbitControls.js';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader.js';
import TWEEN, { Group } from '@tweenjs/tween.js'
import { socket } from "./socketio.js";
import {Text} from 'troika-three-text';


import { faceDetected } from './facedetection.js';

let camera, controls, scene, renderer, origin, yAxis, raycaster, pointer, fog;
let intersections = [];
let previousIntersection;
let previousHoverIntersection;
let objects = [];

let labelComplexityPivot;

let openProjectHelper1;
let openProjectHelper2;

const degree = Math.PI/180;

let cameraPositionIdle;
let cameraPositionInit;
let cameraPositionStorage;
let cameraPositionStorageHelper;
let faceHasLeftOnce = false;
let faceIsVisible = false;

export let isMouseDrag = false;


export function init() {

  // scene

  scene = new THREE.Scene();

  scene.fog = new THREE.Fog(0xf0f0f0, 2.5, 6);

  renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.toneMapping = THREE.ACESFilmicToneMapping;
  renderer.toneMappingExposure = 3;
  render.outputEncoding = THREE.sRGBEncoding;
  document.body.appendChild( renderer.domElement );

  // camera

  cameraPositionIdle = new THREE.Vector3(8, 0, 1);
  cameraPositionInit = new THREE.Vector3(2.5, 0, 1);
  cameraPositionStorage = new THREE.Vector3();
  cameraPositionStorageHelper = new THREE.Vector3();

  camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, .01, 100 );
  camera.position.set( cameraPositionIdle.x, cameraPositionIdle.y, cameraPositionIdle.z );

  // controls

  controls = new OrbitControls( camera, renderer.domElement );
  controls.listenToKeyEvents( window );

  controls.enablePan = false;
  controls.enableDamping = true;
  controls.dampingFactor = 0.05;
  controls.autoRotate = true;
  controls.autoRotateSpeed = .5;

  controls.screenSpacePanning = false;

  controls.minDistance = .1;
  controls.maxDistance = 10;

  controls.minPolarAngle = 10*degree;
  controls.maxPolarAngle = 170*degree;

  // vectors

  origin = new THREE.Vector3(0, 0, 0);
  yAxis = new THREE.Vector3(0, 1, 0);

  // raycaster

  raycaster = new THREE.Raycaster(
    new THREE.Vector3(), // origin
    new THREE.Vector3(0, -1, 0), // direction
  );

  pointer = new THREE.Vector2();

  openProjectHelper1 = new THREE.Object3D();
  openProjectHelper2 = new THREE.Object3D();

  // hdri

  new RGBELoader()
  .load('../assets/threejs/quattro_canti_1k.hdr', function(texture) {
    texture.mapping = THREE.EquirectangularReflectionMapping;
    scene.environment = texture;
  })

  // gltf

  const gltfMaterial = new THREE.MeshStandardMaterial({
    color: 0x4a4744,
    metalness: 1,
    roughness: .2,
  });

  const loader = new GLTFLoader();
  loader.load('../assets/threejs/graph_5.glb',
    function ( gltf ) {
      let model = gltf.scene;
      model.traverse((o) => {
        if (o.isMesh) o.material = gltfMaterial;
      });
      scene.add( gltf.scene );
    },
    function ( xhr ) {
      console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
    },
    function ( error ) {
      console.log( 'GLTF load failed' );
    }
  );

  const linesMaterial = new THREE.LineDashedMaterial( {
    color: 0x000000,
    linewidth: 1,
    scale: 50,
    dashSize: .7,
    gapSize: .4,
    opacity: .25,
    transparent: true
  } );
  
  for (let i = 0; i < 3; i++) {
    const curve = new THREE.CubicBezierCurve3(
      new THREE.Vector3( .005, .995, 0 ),
      new THREE.Vector3( .005, .6, 0 ),
      new THREE.Vector3( .6, .005, 0 ),
      new THREE.Vector3( .995, .005, 0 ),
    );
    
    const points = curve.getPoints( 20 );
  
    let geometry = new THREE.BufferGeometry().setFromPoints(points);
    
    let line = new THREE.Line(geometry, linesMaterial);
    line.computeLineDistances();

    rotateAroundPoint(line, origin, yAxis, 60*degree + 120*degree*i);

    scene.add(line);
  }

  for (let i = 0; i < 3; i++) {
    const curve = new THREE.CubicBezierCurve3(
      new THREE.Vector3( .005, -.995, 0 ),
      new THREE.Vector3( .005, -.6, 0 ),
      new THREE.Vector3( .6, -.005, 0 ),
      new THREE.Vector3( .995, -.005, 0 ),
    );
    
    const points = curve.getPoints( 20 );
  
    let geometry = new THREE.BufferGeometry().setFromPoints(points);
    
    let line = new THREE.Line(geometry, linesMaterial);
    line.computeLineDistances();

    rotateAroundPoint(line, origin, yAxis, 60*degree + 120*degree*i);

    scene.add(line);
  }

  // text

  let labelGroups = [];
  let labels = ['Above', 'Between', 'Connected'];

  const labelMaterial = new THREE.MeshBasicMaterial({
    color: 0x000000,
    side: THREE.FrontSide
  });

  for (let i = 0; i < 3; i++) {
    let labelGroup = new THREE.Group();
    
    let front = new Text();
    let back = new Text();

    let sides = [front, back];
    
    for (let j = 0; j < sides.length; j++) {
      sides[j].text = labels[i];
      sides[j].font = '/assets/fonts/HelveticaNeueLTStd-Bd.otf';
      sides[j].fontSize = .02;
      sides[j].material = labelMaterial;
      
      if (j == 1) {
        sides[j].anchorX = 'left';
        sides[j].rotateY(Math.PI);
      } else {
        sides[j].anchorX = 'right';
      }

      labelGroup.add(sides[j]);
    }

    labelGroup.position.set(1, .04, 0);
    rotateAroundPoint(labelGroup, origin, yAxis, 60*degree + 120*degree*i);
    
    scene.add(labelGroup);
    labelGroups.push(labelGroup);
  }

  labelComplexityPivot = new THREE.Object3D();

  let labelComplex = new Text();
  labelComplex.text = 'Complex';
  labelComplex.font = '/assets/fonts/HelveticaNeueLTStd-Bd.otf';
  labelComplex.fontSize = .02;
  labelComplex.anchorX = 'right';
  labelComplex.material = labelMaterial;

  labelComplex.rotateZ(Math.PI/2);
  labelComplex.position.set(.01, .98, 0);
  
  labelComplexityPivot.add(labelComplex);
  
  let labelSimple = new Text();
  labelSimple.text = 'Simple';
  labelSimple.font = '/assets/fonts/HelveticaNeueLTStd-Bd.otf';
  labelSimple.fontSize = .02;
  labelSimple.anchorX = 'left';
  labelSimple.material = labelMaterial;
  
  labelSimple.rotateZ(Math.PI/2);
  labelSimple.position.set(.01, -.98, 0);

  labelComplexityPivot.add(labelSimple);

  
  scene.add(labelComplexityPivot);

  // textures

  // const texture = new THREE.TextureLoader().load('../assets/renderings/sp-0.jpg' );

  // rendering

  // const renderingGeometry = new THREE.PlaneGeometry(.15, .2);
  // const renderingMaterialFront = new THREE.MeshBasicMaterial( {
  //   map: texture,
  //   side: THREE.BackSide
  // } );
  // const renderingMaterialBack = new THREE.MeshPhongMaterial( {
  //   color: 0xffffff, 
  // } );

  // for (let i = 0; i < 3; i++) {
  //   const renderingGroup = new THREE.Group();
  //   renderingGroup.position.set(.6, 0, 0);
  //   rotateAroundPoint(renderingGroup, origin, yAxis, degree * 120 * i + 60 * degree);
  //   renderingGroup.lookAt(0, 0, 0);
  //   renderingGroup.updateMatrix();
  //   renderingGroup.matrixAutoUpdate = false;
  //   scene.add(renderingGroup);
  //   const renderingMeshFront = new THREE.Mesh( renderingGeometry, renderingMaterialFront );
  //   const renderingMeshBack = new THREE.Mesh( renderingGeometry, renderingMaterialBack );
  //   renderingGroup.add( renderingMeshFront );
  //   renderingGroup.add( renderingMeshBack );
  // }

  // lights

  // const ambientLight = new THREE.AmbientLight( 0xFFFFFF );
  // scene.add(ambientLight);

  window.addEventListener( 'resize', onWindowResize );
}

function rotateAroundPoint(obj, point, axis, theta, pointIsWorld){
  pointIsWorld = (pointIsWorld === undefined)? false : pointIsWorld;

  if(pointIsWorld){
    obj.parent.localToWorld(obj.position);
  }

  obj.position.sub(point);
  obj.position.applyAxisAngle(axis, theta);
  obj.position.add(point);

  if(pointIsWorld){
    obj.parent.worldToLocal(obj.position);
  }

  obj.rotateOnAxis(axis, theta);
}

function onWindowResize() {

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );

}

export function animate() {

  requestAnimationFrame( animate );

  if (camera.position.y > 1) {
    camera.position.y = 1;
  } else if (camera.position.y < -1) {
    camera.position.y = -1;
  }

  labelComplexityPivot.quaternion.copy(camera.quaternion);

  controls.target.y = camera.position.y;
  controls.update();

  render();
  TWEEN.update();
}

function render() {

  renderer.render( scene, camera );

}

export function raycasting(event, trigger) {
  pointer.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	pointer.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

  raycaster.setFromCamera(pointer, camera);

  intersections = raycaster.intersectObjects(scene.children);

  let intersection;

  for (const item of intersections) {
    if (item.object.parent.userData.type) {
      intersection = item;
      break;
    }
  }

  if (trigger == 'click') {
    if (!intersection) {
      if (previousIntersection) {
        socket.emit('closeProjectOut', {project: previousIntersection.userData.id});

        previousIntersection = null;
      }
      controls.autoRotateSpeed = .75;

      camera.near = .01;
      camera.updateProjectionMatrix();
    }
  }

  if (trigger == 'dragStart') {
    controls.autoRotateSpeed = .75;
    
    if (previousIntersection) {
      socket.emit('closeProjectOut', {project: previousIntersection.userData.id});

      previousIntersection = null;
    }
  }

  if (trigger == 'wheel') {
    controls.autoRotateSpeed = .75;
    
    if (previousIntersection) {
      socket.emit('closeProjectOut', {project: previousIntersection.userData.id});

      previousIntersection = null;
    }
  }

  if (intersection) {

    if (trigger == 'click') {
      controls.autoRotateSpeed = 0;
      
      let group = intersection.object.parent;

      if (previousIntersection && previousIntersection != group) {
        socket.emit('changeProjectOut', {project: previousIntersection.userData.id});
      }

      let inFrontOfProject = new THREE.Vector3();
      inFrontOfProject.copy(group.position);
      
      let projectWorldDirection = new THREE.Vector3();
      group.getWorldDirection(projectWorldDirection);
      projectWorldDirection.normalize();

      inFrontOfProject = inFrontOfProject.add(projectWorldDirection.multiplyScalar(.20));

      new TWEEN.Tween(camera.position)
        .to(inFrontOfProject, 1000)
        .easing(TWEEN.Easing.Cubic.InOut)
        .onStart(function() {
        })
        .onComplete(function() {
        })
        .start();

      socket.emit('openProjectOut', {project: group.userData.id});
      
      previousIntersection = group;
    }
    
    if (trigger == 'hover') {
      if (previousHoverIntersection && previousHoverIntersection != intersection.object.parent) {
        
        if (previousHoverIntersection != intersection.object.parent) {
          let group = intersection.object.parent;
          
          previousHoverIntersection = group;
        }
      }
    }
    
    return intersection.object.parent.userData.id;
  } else {
    if (previousHoverIntersection) {
     
    }
    previousHoverIntersection = null;

    return;
  }
}

export function setupProject(project) {
  let angle;

  let randomDegreesMin = -25;
  let randomDegreesMax = 25;

  let randomDegrees = Math.random() * (randomDegreesMax - randomDegreesMin) + randomDegreesMin;
  randomDegrees = randomDegrees * degree;

  let randomXOffsetMin = -.5;
  let randomXOffsetMax = .5;

  let randomXOffset = Math.random() * (randomXOffsetMax - randomXOffsetMin) + randomXOffsetMin;

  let randomYOffsetMin = -.1;
  let randomYOffsetMax = .1;

  let randomYOffset = Math.random() * (randomYOffsetMax - randomYOffsetMin) + randomYOffsetMin;

  if (project.placement == 'A') {
    angle = 60 * degree + randomDegrees;
  } else if (project.placement == 'B') {
    angle = 180 * degree + randomDegrees;
  } else if (project.placement == 'C') {
    angle = 300 * degree + randomDegrees;
  }
  
  // textures

  const renderingTexture = new THREE.TextureLoader().load(`../assets/renderings/${project.title}.jpg` );

  // rendering

  const renderingGeometry = new THREE.PlaneGeometry(.09, .12);
  const renderingMaterialFront = new THREE.MeshBasicMaterial( {
    map: renderingTexture,
    toneMapped: false
    // wireframe: true
  } );
  const renderingMaterialBack = new THREE.MeshBasicMaterial( {
    color: 0x888888,
    // map: renderingTexture,
    side: THREE.BackSide,
    // wireframe: true
  } );

  const renderingGroup = new THREE.Group();
  renderingGroup.position.set(.6 + randomXOffset, 0, 0);
  rotateAroundPoint(renderingGroup, origin, yAxis, angle);
  renderingGroup.lookAt(0, 0, 0);
  renderingGroup.translateY(((project.complexity / 5) - 1) + randomYOffset);
  renderingGroup.rotateY(Math.PI);
  // renderingGroup.updateMatrix();
  // renderingGroup.matrixAutoUpdate = false;
  const renderingMeshFront = new THREE.Mesh( renderingGeometry, renderingMaterialFront );
  const renderingMeshBack = new THREE.Mesh( renderingGeometry, renderingMaterialBack );
  renderingGroup.add( renderingMeshFront );
  renderingGroup.add( renderingMeshBack );

  renderingGroup.userData.type = 'rendering';
  renderingGroup.userData.id = project.index;
  scene.add(renderingGroup);
}

export function faceEnter() {
  console.log('face enter');
  if (faceHasLeftOnce) {
    new TWEEN.Tween(camera.position)
    .to({
      x: cameraPositionInit.x,
      y: cameraPositionInit.y,
      z: cameraPositionInit.z
    }, 2000)
    .easing(TWEEN.Easing.Cubic.InOut)
    .onStart(function() {

    })
    .onComplete(function() {

    })
    .start();
  }
}

export function faceLeave() {
  let dollyBackCamera = new THREE.Vector3();
  dollyBackCamera.copy(camera.position);
  
  let cameraWorldDirection = new THREE.Vector3();
  camera.getWorldDirection(cameraWorldDirection);
  cameraWorldDirection.normalize();

  dollyBackCamera = dollyBackCamera.add(cameraWorldDirection.multiplyScalar(-3));
  
  console.log('face leave');

  new TWEEN.Tween(camera.position)
    .to({
      x: cameraPositionIdle.x,
      y: cameraPositionIdle.y,
      z: cameraPositionIdle.z
    }, 3000)
    .easing(TWEEN.Easing.Cubic.InOut)
    .onStart(function() {
      // cameraPositionStorageHelper.copy(camera.position);
    })
    .onComplete(function() {
      // cameraPositionStorage.copy(cameraPositionStorageHelper);
    })
    .start();

  faceHasLeftOnce = true;
}

export function bringCameraIntoPosition() {
  new TWEEN.Tween(camera.position)
    .to({
      x: cameraPositionInit.x,
      y: cameraPositionInit.y,
      z: cameraPositionInit.z
    }, 2000)
    .easing(TWEEN.Easing.Cubic.InOut)
    .onStart(function() {
      
    })
    .onComplete(function() {

    })
    .start();
}
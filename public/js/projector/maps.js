import { projects } from './projector.js'

let satelliteWindow = document.querySelector('#satelliteWindow');
let satellite = document.querySelector('#satellite');

// const mapCoordinates = {
//   tl: {y: 48.272330, x: 16.282738},
//   bl: {y: 48.141900, x: 16.282738},
//   br: {y: 48.141900, x: 16.478468},
//   tr: {y: 48.272330, x: 16.478468}
// }

export function faceEnter(id) {

}

export function faceLeave(id) {
  satellite.style.transform = `scale(1) translate3D(0%, 0%, 0)`;
}

export function toProject(id) {
  // let x = (Math.random() * 20) - 10;
  // let y = (Math.random() * 20) - 10;

  let x = projects[id].translate.x;
  let y = projects[id].translate.y;

  satellite.style.transform = `scale(10) translate3D(${x}%, ${y}%, 0)`;
}

export function fromProject(id) {
  
}
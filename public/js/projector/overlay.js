const overlayIn = document.querySelector('#overlayIn');
const overlayOut = document.querySelector('#overlayOut');

export function faceEnter() {
  overlayOut.pause();
  overlayOut.currentTime = 0;
  overlayOut.classList.remove('isVisible');
  overlayIn.classList.add('isVisible');
  overlayIn.play();
}

export function faceLeave() {
  overlayIn.pause();
  overlayIn.currentTime = 0;
  overlayIn.classList.remove('isVisible');
  overlayOut.classList.add('isVisible');
  overlayOut.play();
}
const projection = document.querySelector('#projection');

let zIndex = 1;

const mapCoordinates = {
  longMin: 16.282738, 
  longMax: 16.478468,
  latMin: 48.141900,
  latMax: 48.272330
};

function rangeValue(val, inMin, inMax, outMin, outMax) {
  return (val - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}

export default class Project {
  constructor(properties, index) {
    Object.assign(this, properties);
    this.index = index;
    this.translate = {};
    
    this.format();
    this.setupDrone();
  }

  init() {
    this.generateHtml();
    this.appendHtml();
  }

  format() {
    this.abbr = `SP-${this.placement}-${this.index.toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping: false})}`;

    this.position = this.position.split(",").map(item => item.trim());

    this.translate.y = rangeValue(this.position[0], mapCoordinates.latMin, mapCoordinates.latMax, -50, 50);
    this.translate.x = rangeValue(this.position[1], mapCoordinates.longMin, mapCoordinates.longMax, 50, -50);
  };

  setupDrone() {
    if (this.video == 'X') {
      console.log(this.title);
      this.drone = this.title + '.mp4';
    } else if (this.video) {
      this.drone = this.video + '.mp4';
    } else {
      this.drone = 'a-1.mp4';
    }
  }

  generateHtml() {

    this.html = {};

    this.html.title = document.createElement('h3');
    this.html.title.classList = 'project__title';
    this.html.title.innerHTML = this.title;

    this.html.placement = document.createElement('span');
    this.html.placement.classList = 'project__placement';
    this.html.placement.innerHTML = this.placement;

    this.html.videoWindow = document.createElement('div');
    this.html.videoWindow.classList = 'project__window';

    this.html.video = document.createElement('video');
    this.html.video.classList = 'project__video';
    this.html.video.src = `./assets/drone/${this.drone}`;
    this.html.video.muted = true;
    this.html.video.loop = true;
    this.html.video.preload = 'metadata';
    this.html.videoWindow.appendChild(this.html.video);

    this.html.wrapper = document.createElement('div');
    this.html.wrapper.classList = 'project';

    this.html.wrapper.appendChild(this.html.title);
    this.html.wrapper.appendChild(this.html.placement);
    this.html.wrapper.appendChild(this.html.videoWindow);
  }

  appendHtml() {
    let fragment = document.createDocumentFragment();
    fragment.appendChild(this.html.wrapper)
    projection.appendChild(fragment);
  }

  openProject() {
    this.html.wrapper.style.zIndex = zIndex;
    zIndex++;
    let previousProject = document.querySelector('.project.isOpen');
    if (previousProject) {
      setTimeout(function() {
        previousProject.classList.remove('isOpen')
      }, 1000);
    }
    this.html.wrapper.classList.add('isOpen');
  }
  
  closeProject() {
    this.html.wrapper.classList.remove('isOpen');
  }
}
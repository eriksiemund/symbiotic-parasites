import Project from './project.js';
import { socket } from './socketio.js'

export let projects = [];

fetch('../json/projects.json').then(response => {
  
  return response.json();

}).then(items => {  
  
  for (let index = 0; index < items.length; index++) {
    let project = new Project(items[index], index);
    projects.push(project);
  }

  for (const project of projects) {
    project.init();
  }

}).catch(error => {
  console.error(error);
})


let h1 = document.querySelector('h1');
let audio = document.querySelector('audio');

h1.addEventListener('click', function() {
  audio.play();
})
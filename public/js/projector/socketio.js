import Project from './project.js';
import { projects } from './projector.js'
import { faceEnter as faceEnterCSS, faceLeave as faceLeaveCSS } from "./style.js";
import { faceEnter as faceEnterSound, faceLeave as faceLeaveSound, playProject as playProjectSound, stopProject as stopProjectSound } from "./sound.js";
import { faceEnter as faceEnterVideo, faceLeave as faceLeaveVideo, playProject as playProjectVideo, stopProject as stopProjectVideo } from "./video.js";
import { faceEnter as faceEnterOverlay, faceLeave as faceLeaveOverlay } from "./overlay.js";
import { faceEnter as faceEnterMaps, faceLeave as faceLeaveMaps, toProject as toProjectMaps, fromProject as fromProjectMaps } from "./maps.js";

export const socket = io();

setTimeout(function() {
  faceEnterSound();
  faceEnterVideo();
  faceEnterOverlay();
  faceEnterCSS();
}, 2000);

socket.on('faceEnterProjectorIn', (data) => {
  console.log('enter');
  faceEnterSound();
  faceEnterVideo();
  faceEnterOverlay();
  faceEnterCSS();
})

socket.on('faceLeaveProjectorIn', (data) => {
  faceLeaveSound();
  faceLeaveVideo();
  faceLeaveOverlay();
  faceLeaveCSS();
})

socket.on('openProjectIn', (data) => {
  projects[data.project].openProject();
  playProjectSound(data.project);
  playProjectVideo(data.project);
  toProjectMaps(data.project);
})

socket.on('closeProjectIn', (data) => {
  projects[data.project].closeProject();
  stopProjectSound(data.project);
  stopProjectVideo(data.project);
  fromProjectMaps(data.project);
})

socket.on('changeProjectIn', (data) => {
  setTimeout(function() {
    projects[data.project].closeProject();
  }, 1000);
  stopProjectSound(data.project);
  stopProjectVideo(data.project);
})


import { projects } from './projector.js'

const activateMedia = document.querySelector('#activateMedia');
const toOne = document.querySelector('#toOne');
const toZero = document.querySelector('#toZero');


let isMediaActive = false;

let samplePaths = [
  {
    key: 'a-1',
    path: './assets/sounds/a-1.mp3'
  },
  {
    key: 'a-2',
    path: './assets/sounds/a-2.mp3'
  },
  {
    key: 'a-5',
    path: './assets/sounds/a-5.mp3'
  },
  {
    key: 'a-8',
    path: './assets/sounds/a-8.mp3'
  },
  {
    key: 'a-9',
    path: './assets/sounds/a-9.mp3'
  },
  {
    key: 'a-10',
    path: './assets/sounds/a-10.mp3'
  },
  {
    key: 'a-11',
    path: './assets/sounds/a-11.mp3'
  },
  {
    key: 'a-12',
    path: './assets/sounds/a-12.mp3'
  },
  {
    key: 'a-13',
    path: './assets/sounds/a-13.mp3'
  },
  {
    key: 'a-15',
    path: './assets/sounds/a-15.mp3'
  },
  {
    key: 'a-16',
    path: './assets/sounds/a-16.mp3'
  },
  {
    key: 'a-17',
    path: './assets/sounds/a-17.mp3'
  },
  {
    key: 'a-18',
    path: './assets/sounds/a-18.mp3'
  },
  {
    key: 'a-19',
    path: './assets/sounds/a-19.mp3'
  },
  {
    key: 'a-20',
    path: './assets/sounds/a-20.mp3'
  },
  {
    key: 'a-22',
    path: './assets/sounds/a-22.mp3'
  },
  {
    key: 'a-23',
    path: './assets/sounds/a-23.mp3'
  },
  {
    key: 'a-24',
    path: './assets/sounds/a-24.mp3'
  },
  {
    key: 'a-25',
    path: './assets/sounds/a-25.mp3'
  },
  {
    key: 'a-28',
    path: './assets/sounds/a-28.mp3'
  },
  {
    key: 'a-29',
    path: './assets/sounds/a-29.mp3'
  },
  {
    key: 'a-30',
    path: './assets/sounds/a-30.mp3'
  },
  {
    key: 'a-31',
    path: './assets/sounds/a-31.mp3'
  },
  {
    key: 'a-32',
    path: './assets/sounds/a-32.mp3'
  },
  {
    key: 'a-33',
    path: './assets/sounds/a-33.mp3'
  },
  {
    key: 'a-34',
    path: './assets/sounds/a-34.mp3'
  },
  {
    key: 'a-35',
    path: './assets/sounds/a-35.mp3'
  },
  {
    key: 'b-1',
    path: './assets/sounds/b-1.mp3'
  },
  {
    key: 'b-2',
    path: './assets/sounds/b-2.mp3'
  },
  {
    key: 'b-5',
    path: './assets/sounds/b-5.mp3'
  },
  {
    key: 'b-6',
    path: './assets/sounds/b-6.mp3'
  },
  {
    key: 'b-10',
    path: './assets/sounds/b-10.mp3'
  },
  {
    key: 'b-11',
    path: './assets/sounds/b-11.mp3'
  },
  {
    key: 'b-14',
    path: './assets/sounds/b-14.mp3'
  },
  {
    key: 'b-17',
    path: './assets/sounds/b-17.mp3'
  },
  {
    key: 'b-18',
    path: './assets/sounds/b-18.mp3'
  },
  {
    key: 'b-21',
    path: './assets/sounds/b-21.mp3'
  },
  {
    key: 'b-22',
    path: './assets/sounds/b-22.mp3'
  },
  {
    key: 'b-25',
    path: './assets/sounds/b-25.mp3'
  },
  {
    key: 'b-27',
    path: './assets/sounds/b-27.mp3'
  },
  {
    key: 'b-29',
    path: './assets/sounds/b-29.mp3'
  },
  {
    key: 'c-1',
    path: './assets/sounds/c-1.mp3'
  },
  {
    key: 'c-2',
    path: './assets/sounds/c-2.mp3'
  },
  {
    key: 'c-3',
    path: './assets/sounds/c-3.mp3'
  },
  {
    key: 'c-5',
    path: './assets/sounds/c-5.mp3'
  },
  {
    key: 'c-9',
    path: './assets/sounds/c-9.mp3'
  },
  {
    key: 'c-10',
    path: './assets/sounds/c-10.mp3'
  },
  {
    key: 'c-11',
    path: './assets/sounds/c-11.mp3'
  },
  {
    key: 'c-12',
    path: './assets/sounds/c-12.mp3'
  },
  {
    key: 'c-14',
    path: './assets/sounds/c-14.mp3'
  },
  {
    key: 'c-18',
    path: './assets/sounds/c-18.mp3'
  },
  {
    key: 'c-21',
    path: './assets/sounds/c-21.mp3'
  },
];

const audioCtx = new AudioContext();
let samples;
let playing = [];
let playingPrevious;

activateMedia.addEventListener('click', function() {
  
  if (audioCtx.state == "suspended") {
    audioCtx.resume();
  }

  isMediaActive = true;
  activateMedia.remove();
})

toOne.addEventListener('click', function() {
  // gainNode.gain.linearRampToValueAtTime(1.0, audioCtx.currentTime + 2);
})

toZero.addEventListener('click', function() {
  // gainNode.gain.linearRampToValueAtTime(0, audioCtx.currentTime + 2);
  stopSample(playing);
})

async function getFile(filePath) {
  const response = await fetch(filePath);
  const arrayBuffer = await response.arrayBuffer();
  const audioBuffer = await audioCtx.decodeAudioData(arrayBuffer);

  return audioBuffer;
}

async function setupSamples(paths) {
  const audioBuffers = [];

  for (let path of paths) {
    let sample = await getFile(path.path);

    let object = {
      key: path.key,
      sample: sample
    }
    audioBuffers.push(object);
  }

  return audioBuffers;
}

function playSample(audioBuffer) {
  const sampleSource = audioCtx.createBufferSource();
  sampleSource.buffer = audioBuffer.sample;

  let gainNode = audioCtx.createGain();
  
  gainNode.gain.setValueAtTime(0, audioCtx.currentTime);
  gainNode.gain.linearRampToValueAtTime(1, audioCtx.currentTime + 2);

  gainNode.connect(audioCtx.destination);
  sampleSource.connect(gainNode);
  
  sampleSource.start(0);

  return [sampleSource, gainNode];
}

function stopSample(playing) {
  let sampleSource = playing[0];
  let gainNode = playing[1];
  gainNode.gain.setValueAtTime(1, audioCtx.currentTime);
  gainNode.gain.linearRampToValueAtTime(0, audioCtx.currentTime + 2);

  setTimeout(function() {
    sampleSource.stop();
    gainNode.disconnect();
    sampleSource.disconnect();
  }, 2000);
}

setupSamples(samplePaths).then(function(response) {
  samples = response;
  console.log('sound-ready');
  console.log(samples);
})






export function faceEnter() {
  if (isMediaActive) {
  }
}

export function faceLeave() {
  if (isMediaActive) {
  }
}

let sampleCounter = 0;

export function playProject(id) {
  if (isMediaActive) {
    let key;

    console.log(projects[id].sound);

    if (projects[id].sound == 'X') {
      key = projects[id].title;
    } else if (projects[id].sound) {
      key = projects[id].sound;
    } else {
      key = 'a-39';39    }

    // console.log(key);

    let wrapper = samples.find(obj => {
      return obj.key === key
    })

    console.log(wrapper);

    playing = playSample(wrapper);
    sampleCounter++;

    // setTimeout(function() {
    //   playingPrevious = playing;
    // }, 100);
  }
}

export function stopProject(id) {
  if (isMediaActive) {
    stopSample(playing);
  }
}
const body = document.querySelector('body');
const h1 = document.querySelector('h1');

export function faceEnter() {
  body.classList.add('isEntered');
}

export function faceLeave() {
  body.classList.remove('isEntered');
}
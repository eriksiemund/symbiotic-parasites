import { projects } from './projector.js'

const activateMedia = document.querySelector('#activateMedia');

let isMediaActive = false;

let playing;
let playingPrevious;

activateMedia.addEventListener('click', function() {
  isMediaActive = true;
})

function playVideo(id) {
  let video = projects[id].html.video;
  video.play();

  return video;
}

function stopVideo(playing) {
  let video = playing;
  
  setTimeout(function() {
    video.pause();
    video.currentTime = 0;
  }, 2000);
}



export function faceEnter() {
  if (isMediaActive) {
  }
}

export function faceLeave() {
  if (isMediaActive) {
  }
}

export function playProject(id) {
  if (isMediaActive) {
    playing = playVideo(id);
  }
}

export function stopProject(id) {
  if (isMediaActive) {
    stopVideo(playing);
  }
}
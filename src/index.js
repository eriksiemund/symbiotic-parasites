const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const {Server} = require('socket.io');
const io = new Server(server);
const PORT = process.env.PORT || 3002;

app.use(express.static('public'));

io.on('connect', (socket) => {
  console.log('a user connected');

  socket.on('faceEnterProjectorOut', (data) => {    
    socket.broadcast.emit('faceEnterProjectorIn', {isEntered: data.isEntered});
  })

  socket.on('faceLeaveProjectorOut', (data) => {    
    socket.broadcast.emit('faceLeaveProjectorIn', {isEntered: data.isEntered});
  })

  socket.on('openProjectOut', (data) => {    
    socket.broadcast.emit('openProjectIn', {project: data.project});
  })

  socket.on('changeProjectOut', (data) => {    
    socket.broadcast.emit('changeProjectIn', {project: data.project});
  })

  socket.on('closeProjectOut', (data) => {    
    socket.broadcast.emit('closeProjectIn', {project: data.project});
  })
})

server.listen(PORT, () => {
  console.log(`listening on *:${PORT}`);
})
const path = require('path');
const WebpackBundleAnalyzer = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
let devMode = process.env.devMode || true;
const NodemonPlugin = require('nodemon-webpack-plugin');

module.exports = {
  entry: {
    main: './public/js/desktop/index.js',
    projector: './public/js/projector/projector.js'
  },
  mode: devMode ? "development" : "production",
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'public/dist')
  },
  devServer: {
    static: path.join(__dirname, 'public'),
    port: 3002
  },
  plugins: [
    new NodemonPlugin({
      script: './src/index.js',

      watch: path.resolve('./public'),
    })
  ]
}